__author__ = 'sjmaharjan'

import codecs
def merge_output_files(result_file,test_file, output_merge_file):
    with codecs.open(output_merge_file,mode='wb',encoding='utf-8') as out:
        with codecs.open(result_file,mode='rb',encoding='utf-8') as f1:
             with codecs.open(test_file,mode='rb',encoding='utf-8') as f2:
                 for line1,line2  in zip(f1.readlines(),f2.readlines()):
                     line1= line1.rstrip('\r\n')
                     line2= line2.rstrip('\r\n')
                     out.write(line2+"\t"+line1.split('\t')[1] +'\n')





if __name__=="__main__":
    result_file='/Users/sjmaharjan/numerical_project/iter_20_model/results/result.txt'
    test_file='/Users/sjmaharjan/crf/data/nepali-english/test/nepali-english-final-test-data.tsv'
    output_merge_file='/Users/sjmaharjan/numerical_project/iter_20_model/results/result_merged.txt'
    merge_output_files(result_file,test_file,output_merge_file)





