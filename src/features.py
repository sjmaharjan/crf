__author__ = 'suraj'

import string

from collections import defaultdict
from collections import Counter
from scipy.sparse import csr_matrix

import data
import numpy as np
import functools

def feature_word_bi_grams():
    return lambda words, previous_label, current_label, position: Counter(
        [words[position - 1] + " " + words[position]+":"+current_label if position > 0 else  "* " + words[position]+":"+current_label])


def feature_capital():
    return lambda words, previous_label, current_label, position: Counter(['CAPITAL'+":"+current_label]) if words[position][
                                                                                              0].isupper() and position >= 0 else Counter()


def feature_prefixlt4():
    return lambda words, previous_label, current_label, position: Counter([words[position][0:i]+":"+current_label for i in
                                                                           xrange(1,
                                                                                  min(5,
                                                                                      len(words[position])))])


def feature_word_tag():
    return lambda words, previous_tag, current_tag, position: Counter([words[position] + ":" + current_tag])


def feature_sufixlt4():
    return lambda words, previous_label, current_label, position: Counter([words[position][-i:]+":"+current_label for i in
                                                                           xrange(1,
                                                                                  min(5,
                                                                                      len(words[position])))])


def feature_starts_with_punct():
    return lambda words, previous_label, current_label, position: Counter(["PUNCTUATION"+":"+current_label]) if \
    words[position][0] in string.punctuation else Counter()


def memoize(fx):
    cache = {}
    @functools.wraps(fx)
    def memoizer(*args, **kwargs):
        # print type(args)
        # print args
        if args not in cache:
            print "not in cache", len(cache)
            val = fx(*args, **kwargs)
            cache[args] = val
            return val
        # print "in cache", len(cache)
        return cache[args]
    return memoizer


class Features(object):
    def __init__(self, training_data, feature_fx_lst):
        self.training_data = training_data
        self.feature_fxs = feature_fx_lst
        self.initialize_feature_vectors()


    def initialize_feature_vectors(self):
        f_index = defaultdict()
        f_index.default_factory = f_index.__len__
        # set feature to integer index
        for datum in self.training_data:
            f = self.get_features(datum)
            datum.set_features(f)
            # populate the vocabulary
            for feature_name, value in f.iteritems():
                f_index[feature_name]
        self.no_of_features = len(f_index)
        self.feature_index= dict(f_index)


    def get_features_map(self):
        return self.feature_index

    def get_features(self, datum):
        f = Counter()
        for i, word in enumerate(datum.words):
            if i == 0:
                previous_label = '*'
            else:
                previous_label = datum.labels[i - 1]
            f += self.extract_features(datum.words, previous_label, datum.labels[i], i)
        return f


    def extract_features(self, words, previous_label, current_label, position):
        features = Counter()
        for feature in self.feature_fxs:
            # print words, len(words), position
            features += feature(words, previous_label, current_label, position)
        return features

    @memoize
    def feature_vector(self, datum, previous_label, current_label, position):
        # print "feature6: ", position
        features = self.extract_features(datum.words, previous_label, current_label, position)
        return self._create_vector(features)

    #
    # def feature_vector(self, data):
    # f = self.get_features(data)
    # return self._create_vector(f)


    def _create_vector(self, features):
        feature_v = [0.0] * len(self.feature_index)
        for feature, value in features.iteritems():
            if self.feature_index.has_key(feature):
                feature_v[self.feature_index[feature]] = value
        # print csr_matrix(feature_v).toarray()
        return csr_matrix(feature_v)

    def get_true_label_feature_vectors(self):
        score = Counter()
        for datum in self.training_data:
            score += datum.get_features()
        return self._create_vector(score)


if __name__ == "__main__":
    training_data = data.Data("fjlskjfs")
    datum = data.Datum(["I", "will", "haita"], ["LANG1", "LANG1", "LANG2"])
    training_data.append_data(datum)

    feature = Features(training_data, [feature_capital, feature_starts_with_punct, feature_prefixlt4])
    print feature.get_features_map()
    print datum.get_features()
    print feature.feature_vector(datum)

