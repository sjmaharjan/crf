__author__ = 'suraj'


import codecs

from random import shuffle
class Datum:
    def __init__(self, words, labels):
        self.words = words
        self.labels = labels


    def get_features(self):
        return self.features

    def set_features(self, features):
        self.features = features



class Data():
    def __init__(self, file_path):
        self.data = []
        self.path = file_path
        self.read_data()



    def read_data(self):
        with codecs.open(self.path, 'rb', encoding='utf-8') as source:
            old_tweet_id = None
            old_user_id = None
            words, labels = [], []
            for line in source:
                line = line.rstrip('\r\n')
                if len(line.split('\t')) > 5:  # training data
                    tweet_id, user_id, start, end, token, label = line.split("\t")
                    labels.append(label)
                else:
                    tweet_id, user_id, start, end, token = line.split("\t")

                words.append(token)

                new_tweed_id = tweet_id
                new_user_id = user_id

                if old_tweet_id and old_tweet_id != new_tweed_id:  # new tweet starts
                    self.data.append(Datum(words, labels))
                    words, labels = [], []

                old_tweet_id = new_tweed_id
                old_user_id = new_user_id

            if old_tweet_id != None:
                self.data.append(Datum(words, labels))


    def append_data(self,data):
        self.data.append(data)


    def shuffle_data(self):
        shuffle(self.data)

    def __iter__(self):
        for example in self.data:
            yield example

    def __len__(self):
        return len(self.data)




