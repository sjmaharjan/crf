from collections import defaultdict

__author__ = 'shrprasha'

#k: no of features
#l: learning rate
#n: number of training examples
#m: variable, number of words in a tweet
#labels = ["lang1", "lang2", "other"]


#TODO
#increase speed: features.feature_vector(x, a, b, j) call only once for one value
#what about *, *, 0??? Ans: doesn't exist: first one is *,s,0/1 (according to how it is indexed)

import features
import numpy as np
import math
from scipy.sparse import csr_matrix
from dill import dump, load
import random


class Train:

    def __init__(self, learning_rate, labels, features, tolerance):
        self.labels = labels
        self.learning_rate = learning_rate
        self.features = features
        self.training_data = self.features.training_data
        self.tolerance = tolerance
        self.n = len(self.training_data.data)
        self.weights = csr_matrix((1, self.features.no_of_features))
        self.regularization_parameter = 10
        # self.true_label_scores = load(open('/Users/sjmaharjan/numerical_project/true_labels_nep.dump', "r"))
        # self.true_label_scores = load(open('/Users/sjmaharjan/numerical_project/true_labels_nep_word_tag_only.dump', "r"))

    def update_weights(self):
        # print self.true_label_scores
        self.true_label_scores = self.features.get_true_label_feature_vectors()
        dump(self.true_label_scores,open('/Users/sjmaharjan/numerical_project/true_labels_nep_word_tag_only.dump', 'w'))

        # print "here2: ", self.true_label_scores.toarray()
        # for i in range(0, 10000):
        print "\n\n\n\ndoing --iteration\n\n\n\n"
        self.training_data.shuffle_data()
        for i, datum in enumerate(self.training_data):
            # gradient = self.learning_rate * (self.true_label_scores - self.get_current_label_scores()) #vector subtraction
            # gradient = self.learning_rate * (self.true_label_scores - self.get_current_label_scores_stochastic(datum)) #vector subtraction
            gradient = self.true_label_scores - self.get_current_label_scores_stochastic(datum) - self.regularization_parameter * self.weights #vector subtraction
            # if gradient < x #gradient is a vector
            self.weights = self.weights + self.learning_rate * gradient
            print self.weights.toarray()
            if np.linalg.norm(gradient.toarray(), 2) < self.tolerance:
                break
            print "\n\n\n\niteration "+str(i)+" done\n\n\n\n"

            if i % 5 == 0:
                dump(self.weights, open("/Users/sjmaharjan/numerical_project/model_" + str(i) + ".model", "w"))

        return self.weights

    #not used
    def get_current_label_scores(self):
        #forward-backward
        score = csr_matrix((1, self.features.no_of_features), dtype=np.int8)
        for i, datum in enumerate(self.training_data):
            x = datum.words
            # print "here6: ", len(x), x
            for j in range(0, len(x)-2):
                # print "here5:", j
                for a in self.labels:
                    for b in self.labels:
                        # print "here7: ", j
                        features_list = self.features.feature_vector(datum, a, b, j)
                        # features_list = self.features.feature_vector[x, a, b, j]
                        # print "here8"
                        q_fwd_bkwd = self.get_q_fwd_bkwd(datum, a, b, j) #scalar
                        # print "train11: ", q_fwd_bkwd, features_list.toarray()
                        score = score + q_fwd_bkwd * features_list  #scalar * vector
        return score

    def get_current_label_scores_stochastic(self, datum):
        self.alpha_table = {}
        self.beta_table = {}
        #forward-backward
        score = csr_matrix((1, self.features.no_of_features))
        # for i, datum in enumerate(self.training_data):
        # datum = random.choice(self.training_data.data)
        x = datum.words
        # print "here6: ", len(x), x
        for j in range(0, len(x)-1):
            # print "here5:", j
            for a in self.labels:
                for b in self.labels:
                    # print "here7: ", j
                    features_list = self.features.feature_vector(datum, a, b, j)
                    # features_list = self.features.feature_vector[x, a, b, j]
                    # print "here8"
                    q_fwd_bkwd = self.get_q_fwd_bkwd(datum, a, b, j) #scalar
                    # print "train11: ", q_fwd_bkwd, features_list.toarray()
                    score = score + q_fwd_bkwd * features_list  #scalar * vector
        return score

    def get_shi_value(self, datum, a, b, j):
        # print "train9: ", len(x), j
        prob = self.weights.dot(self.features.feature_vector(datum, a, b, j).transpose())
        # print "here3:", prob[0, 0]
        return prob[0, 0]  #multiply two vectors to get one scalar

    # def calculate_and_store_values(self):
    #     pass
    #
    # def get_values_from_table(self, ):

    def get_q_fwd_bkwd(self, datum, a, b, j):
        # print "train1:", x, a, b
        x = datum.words
        m = len(x)
        #initialize dynamic programming tables
        for s in self.labels:
            self.alpha_table[(0, s)] = self.get_shi_value(datum, "*", s, 0)  #indexed by current_label, current_position
            self.beta_table[(m-1, s)] = 0.0  # math.log(1)
        z_vals = []
        for s in self.labels: #do not join with upper loop
            z_vals.append(self.get_alpha_fwd(datum, m-1, s))
        z = self.logsum(np.array(z_vals))
        alpha = self.get_alpha_fwd(datum, j, a)

        # print "train12: ", j

        shi = self.get_shi_value(datum, a, b, j+1)

        beta = self.get_beta_bkwd(datum, j+1, b)

        mu = alpha + shi + beta #all scalar
        return np.clip(math.exp(mu - z),0.0, 1.0) #scalar (sum of probabilities)



    def get_alpha_fwd(self, datum, j, s):
        if (j, s) not in self.alpha_table:
            tval = []
            for s_prime in self.labels:
                # print "here4 before alpha: ", j, s_prime
                alpha = self.get_alpha_fwd(datum, j-1, s_prime)
                # print "here4 after alpha: ", j, s_prime
                shi = self.get_shi_value(datum, s_prime, s, j)
                # print "train10"
                tval.append(alpha + shi)
            self.alpha_table[(j, s)] = self.logsum(np.array(tval))
        return self.alpha_table[(j, s)]

    def get_beta_bkwd(self, datum, j, s):
        if (j, s) not in self.beta_table:
            tval = []
            for s_prime in self.labels:
                tval.append(self.get_beta_bkwd(datum, j+1, s_prime) + self.get_shi_value(datum, s, s_prime, j+1))
            self.beta_table[(j, s)] = self.logsum(np.array(tval))
        return self.beta_table[(j, s)]

    def logsum(self, l1):
        b = l1.max()
        return b + np.log((np.exp(l1-b)).sum())










