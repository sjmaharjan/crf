from data import Data, Datum
from train_logsum import Train
from test import Viterbi
from features import Features
import codecs
import features
import os

from dill import dump, load

from optparse import OptionParser


def train_crf(train_data_path='', output_dir='', feature_fxs=None, labels=None, learning_rate=0.01, tolerance=1e-3):
    training_data = Data(train_data_path)
    fs = Features(training_data, feature_fxs)
    model = Train(learning_rate, labels, fs, tolerance)
    model_weights = model.update_weights()
    dump(model_weights, open(os.path.join(output_dir, 'model.txt'), "w"))
    dump(fs, open(os.path.join(output_dir, 'features.txt'), "w"))



def predict(test_data_path='', output_dir='', features=None, labels=None, model=None):
    test_data = Data(test_data_path)
    decode = Viterbi(model, features, labels)
    result = []
    with codecs.open(os.path.join(output_dir, 'result.txt'), mode='wb', encoding='utf8') as out:
        for data in test_data:
            seq = decode.decode_sequence(data)
            for word, label in zip(data.words, seq):
                out.write(word + '\t' + label + "\n")

def all_feature_functions():
    return [
       features.feature_word_bi_grams(),
            features.feature_capital(),
            features.feature_prefixlt4(),
            features.feature_sufixlt4(),
            features.feature_starts_with_punct(),
        features.feature_word_tag()
    ]
    # return [features.feature_word_tag()]

if __name__ == "__main__":

    parser = OptionParser()
    parser.add_option("-i", "--input", dest="input",
                      help="path/to/input")
    parser.add_option("-o", "--output", dest="output",
                      help="path/to/output")
    parser.add_option("-m", "--model", dest="model",
                      help="path/to/model")
    parser.add_option("-r", "--run", dest="mode",
                      help="train|test")

    (options, args) = parser.parse_args()

    # feature_fxs = [feature_capital(), feature_prefixlt4(), feature_starts_with_punct()]
    feature_fxs = all_feature_functions()


    # labels = ["lang1", "lang2", "other"]
    labels = ["lang1", "lang2", "other", "ambiguous", "mixed", "ne"]

    if options.mode.lower() == 'train':
        print "Training CRF model"
        train_crf(train_data_path=options.input, output_dir=options.output, feature_fxs=feature_fxs, labels=labels)

        print 'done training'

    elif options.mode.lower() == 'test':
        print "Loading CRF model"
        model_weights = load(open(os.path.join(options.model, 'model.txt'), "r"))
        features = load(open(os.path.join(options.model, 'features.txt'), "r"))
        print 'done loading models'
        predict(test_data_path=options.input, output_dir=options.output, features=features, model=model_weights,
                labels=labels)
        print 'done '
    else:
        print "run mode should be either train or test"



        # # training = data.Data('/Users/suraj/Documents/workspace/crf/crf/data/nepali-english/test/allTweets_test.txt')
        # # training_data = Data('/Users/shrprasha/courses/numerical_analysis/crf/data/nepali-english/training/allTweetsTokenLabels.tsv')
        # training_data = Data(
        # '/Users/shrprasha/courses/numerical_analysis/crf/data/nepali-english/training/allTweetsTokenLabels_short_test.tsv')
        # feature_fxs = [features.feature_capital, features.feature_prefixlt4, features.feature_starts_with_punct]
        # labels = ["lang1", "lang2", "other"]
        # features = Features(training_data, feature_fxs)
        # learning_rate = 0.1
        # # train = Train(learning_rate, labels, features) #TODO
        # # model_weights = train.update_weights()
        # model_weights = csr_matrix(
        # [3.33066907e-17, -1.11022302e-17, -1.44328993e-16, 3.33066907e-17, -3.10862447e-16, 4.44089210e-17,
        # 4.44089210e-17, 9.00000000e-01, 1.33226763e-16, 9.00000000e-01, 1.11022302e-17, 9.00000000e-01,
        # -1.44328993e-16, 9.00000000e-01, 9.00000000e-01, -1.44328993e-16, 1.80000000e+00, 1.11022302e-17,
        #      -3.10862447e-16, -2.66453526e-16, -6.66133815e-17, -3.10862447e-16, 4.44089210e-17, -2.66453526e-16,
        #      -1.44328993e-16, 9.00000000e-01, 1.11022302e-17, 3.33066907e-17, 3.33066907e-17, 8.88178420e-17,
        #      3.33066907e-17, 3.33066907e-17, -2.22044605e-16, 9.00000000e-01, -5.55111512e-17, 1.11022302e-17,
        #      8.88178420e-17, 9.00000000e-01, 9.00000000e-01, 7.77156117e-17, -9.99200722e-17, -6.16297582e-33,
        #      3.33066907e-17, 1.11022302e-17, 8.88178420e-17, -5.55111512e-17, -6.16297582e-33, -5.55111512e-17,
        #      -2.22044605e-16, -2.44249065e-16, -6.16297582e-33, -2.22044605e-16, 6.66133815e-17, 9.00000000e-01,
        #      -5.55111512e-17, 9.00000000e-01, -1.99840144e-16, -1.99840144e-16])
        #
        # test_data = Datum(["@aba", "will", "aba", "gareeko"], [])
        #
        # decode = Viterbi(model_weights, features, labels)
        # print decode.decode_sequence(test_data)
        #
        #
        #
        #
        # # test_dir = '/Users/suraj/Documents/workspace/crf/crf/data/nepali-english/test'

