__author__ = 'suraj'


class Viterbi():
    def __init__(self, model, feature, label_index):
        self.model = model
        self.states = label_index
        self.feature = feature


    def decode_sequence(self, datum):
        pi_table = {}  # probability table
        bp = {}  # stores the sequence of labels backward

        # initialization
        for state in self.states:
            pi_table[(0, state)] = self.model.dot(
                self.feature.feature_vector(datum, '*', state, 0).transpose())[0, 0]  # need to convert to vector
            bp[(0, state)] = '*'

        # compute probability tables and back pointers
        for j in xrange(1, len(datum.words)):
            for s in self.states:
                max_score = float("-infinity")
                for s_dash in self.states:
                    score = pi_table[(j - 1, s_dash)] + self.model.dot(
                        self.feature.feature_vector(datum, s_dash, s, j).transpose())[0, 0]
                    if score > max_score:
                        max_score = score
                        bp[(j, s)] = s_dash
                pi_table[(j, s)] = max_score



        # final sequce
        result = []

        # print bp

        max_score = float("-infinity")
        label = '*'
        n = len(datum.words) - 1
        for s in self.states:
            if (pi_table[(n, s)]) > max_score:
                max_score = pi_table[(n, s)]
                label = s
        result.append(label)

        for i in xrange(n - 1, -1, -1):
            label = bp[(i + 1, label)]
            result.append(label)

        result.reverse()  # reverse the list and return
        return result





